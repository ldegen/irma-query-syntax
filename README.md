# irma-query-syntax

library used by IRMa to parse, rewrite and pretty-print query strings

## Usage

``` coffee

{parse, rewrite, unparse, pretty} = require "@l.degener/irma-query-syntax"

# start with a query string.
query = "+(foo -bar) +oink:'bar baz'"

# parsing the query will produce an abstract syntax tree, which is
# basically an S-expression, represented using arrays and strings.
ast = parse query

# There is a little helper to produce readable output from an ast expression.
# This is mainly useful for debugging.

console.log pretty(ast)

# This will produce the following output:
# 
# [SEQ
#   [MUST, [SEQ
#     [TERM, 'foo']
#     [MUST_NOT, [TERM, 'bar']]]]
#   [MUST, [QLF, 'oink', [SQUOT, 'bar baz']]]]


# Before interpreting the AST, IRMa applies a whole bunch of
# normalizations and simplifications to the ast. The relevant API
# is not 100% stable yet

normalizedAst = rewrite(ast).value

console.log pretty(normalizedAst)

# The result should look like this:
# [SEQ
#   [MUST, [SEQ
#     [SHOULD, [TERM, 'foo']]
#     [MUST_NOT, [TERM, 'bar']]]]
#   [MUST, [QLF, 'oink', [SQUOT, 'bar baz']]]]
#
# Not much happend here, but note the SHOULD occurrence was added before 'bar'

# finally, you can unparse an ast to get a query string:

console.log unparse normalzedAst

# which should print +(?foo -bar) +oink:'bar baz'

```

